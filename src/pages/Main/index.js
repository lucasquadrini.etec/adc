import React, { Component } from 'react';
import NavMain from '../../components/NavMain';
import Banner from '../../components/Banner'
import Nav from '../../components/Nav';

import { Container, WrapperContainer } from './styles';




export default class Main extends Component {
  render() {
    
    return (
    <>
    <NavMain />
    <Banner />
    <WrapperContainer>
    <Container>
    <Nav />
    </Container>
    </WrapperContainer>
    
    </>
    )
  }
}

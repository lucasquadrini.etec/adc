import React from 'react';

import { Text } from './styles';

function Home() {
  return (
    <Text>
        <h2>Uma forma diferente de estudar o direito</h2>
        <h1>Consistente, profunda e completa.</h1>
    </Text>
  );
}

export default Home;
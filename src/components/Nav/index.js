import React, {  } from 'react';
import { Link } from 'react-router-dom';
import { Ul } from './styles';

export default function Nav() {
  
  return (
      <Ul>
        <li><Link to="/" id="Mark" >Home</Link></li>
        <li><Link to="/sobre" >Sobre Nós</Link></li>
        <li><Link to="/blog" >Blog</Link></li>
        <li><Link to="/faq" >FAQ</Link></li>
        <li><Link to="/contato" >Contato</Link></li>
      </Ul>
  );
}

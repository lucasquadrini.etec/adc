import styled from 'styled-components';

export const Container = styled.div`
  
`;
export const Ul = styled.ul`
    
    margin-top: 17px;
    display: flex;
    justify-content: space-between;
    width: 40%;
    font-weight: 600;
    list-style: none;
    font-size: 20px;
    font-family: Verdana, Geneva, Tahoma, sans-serif;
    a{
        text-decoration: none;
        color: #fff;
        padding: 16px 15px;
        white-space: nowrap;
    }
    #Mark{
        
        color: green;
        border-bottom: 10px green solid;
        border-left: 0.02px green solid;
        border-right: 0.02px green solid;

    }


`;

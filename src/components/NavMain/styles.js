import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    background-color: #2A2A2A;
    width: 100%;
    height: 68px;
    padding:0 10px;
    font-size: 17px;
    font-weight: 100;
    box-shadow: 0px 2px 2px 1px rgba(0, 0, 0, 0.9);
`;
export const Title = styled.li`
    display: flex;
    align-items: center;
    color: #8E8E8E;
    list-style: none;
    padding: 0 10px;
    svg{
        margin: 0px 5px;
    }
`;
export const Nav = styled.ul`
    display: flex;
    align-items: center;
    padding: 0
     15px;
    
    
    a{
        text-decoration: none;
        color: #8E8E8E;
        padding: 23.5px 17px;
        border-left: #434343 solid 1px;
        
    }
    button{
        background: none;
        color: #8E8E8E;
        border: none;
        border-left: #434343 solid 1px;
        padding: 23.5px 17px;
        font-size: 17px;
        white-space: nowrap;
    }
    #assinar{
        background: #1B1B1B;
        padding: 23.5px 17px;
        border: none;
    }
`;
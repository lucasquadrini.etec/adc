import React, { Component } from 'react';
import { FaFacebook } from 'react-icons/fa';
import { AiFillTwitterCircle, AiFillYoutube } from 'react-icons/ai';
import { IoLogoInstagram } from 'react-icons/io'
import { SocialMedia, Ex } from './styles';

export default class Sociais extends Component {
  render() {
    return (
           <SocialMedia>
               <a href="www.facebook.com"><FaFacebook size={30} /></a>
               <a href="www.twitter.com"><AiFillTwitterCircle size={33}/></a>
               <Ex href="#"><IoLogoInstagram size={25}/></Ex>
               <Ex  href="#"><AiFillYoutube size={22}/></Ex>
           </SocialMedia>
    );
  }
}
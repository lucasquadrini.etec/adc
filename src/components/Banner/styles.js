import styled from 'styled-components';

export const Container = styled.div`
    background: #FFF;
    display: flex;
    
    justify-content: center;
    height: 252.5px;
    img{
        margin-top: 5%;
        height: 50%;
        width: 20%;
    }
`
import React from 'react';

import Logo from '../../assets/Logo.png'
import { Container } from './styles';

export default function Banner() {
  return (
    <Container>
      <img alt="Logo" src='https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Triangulo.svg/600px-Triangulo.svg.png' />
      <img alt="Logo" src={Logo} />
    </Container>
  );
}

